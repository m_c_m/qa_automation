# QA Automation #

Repo for QA automation interview questions


### Included in this project are 5 Python files: ###

* restful_booker_api.py
* get_booking_test_cases.p
* create_booking_test_cases.py
* update_booking_test_cases.py
* delete_booking_test_cases.py

Each will print results to the screen as well as to a log file called, "REST_API_Test_Log.txt".  All tests can be run with pytest by issuing "pytest *booking_test_cases.py"

get_booking_test_cases.py and create_booking_test_cases.py work well.  The other two have problems due to not being able to get the authentication token to work.
