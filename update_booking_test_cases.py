# These tests operate by first creating a booking and noting the ID, then updating the booking with the ID
# and verifying the results.

# Unfortunately, I am having difficulties incorporating the authentication token to properly update the booking.

import restful_booker_api
import sys
import time

def writeit(*params):						# this function just prints *params to both the console and the log file
	for i in params:
		print(i)
		f.write(str(i))
		#f.write(" \n")
	#print("\n")
	f.write("\n")


try:				# Can the file be opened?
	f = open("REST_API_Test_Log.txt", "a+")					# open the test log in append mode with handle 'f'
except IOError:			# No!  The file could not be opened.
	writeit("Couldn't open REST_API_Test_Log.txt", f)
	sys.exit()			# Die.

writeit("\n\n\n############################################################################")
writeit("This set of tests for the UpdateBooking endpoint was executed at {}.\n".format(time.ctime(time.time())))		# log the time

################### test case 1 for CreateBooking endpoint ###################
writeit("Update only the first name:")
id = restful_booker_api.create_booking("Aria", "Shen", 250, "true", "2020-09-02", "2020-09-03", "view of the lake", False, "0", "i")
writeit(*(restful_booker_api.update_booking("Maxwell", "Shen", 250, "true", "2020-09-02", "2020-09-03", "view of the lake", True, id, "bookings")))

################### test case 2 for CreateBooking endpoint ###################
writeit("\nUpdate all parameters:")
id = restful_booker_api.create_booking("Aria", "Shen", 250, True, "2020-09-02", "2020-09-03", "view of the lake", False, "0", "i")
writeit(*(restful_booker_api.update_booking("Cormac", "McCarthy", 200, "false", "2020-01-01", "2020-01-31", "horse decor", True, id, "bookings")))

