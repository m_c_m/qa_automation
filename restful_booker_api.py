#restful_booker_api.py
import requests
import re
import sys
import json

bookings = []

def response_code(code):		# verifies valid data is returned
	if code != 200:
		print("There was a problem with the response from the server! Status code:", code)
		sys.exit()


def get_authtoken(username = 'admin', password = 'password123'):
    return requests.post('https://restful-booker.herokuapp.com/auth', json={"username" : username, "password" : password}).json()['token']


def get_single_booking(id):
	s = 'https://restful-booker.herokuapp.com/booking/' + str(id)
	response = requests.get(s)
	#response_code(response.status_code)				 # was an error returned?
	return(response.text)


def build_payload(first = "", last = "", totalprice = "", depositpaid = "", checkin = "", checkout = "", additionalneeds = ""):
	# according to restful-booker site, first and last names and checkin and checkout are all optional
	# this builds up the payload based on the parameters passed to the function
	#print("here are the payload params: ", first, last, totalprice, depositpaid, checkin, checkout, additionalneeds)  #DEBUG
	comma_flag = False
	payload = "{"

	if first:
		comma_flag = True
		payload = payload + "\'firstname\': " + "\'" + first + "\'"
	
	if last:
		if comma_flag == False:
			payload = payload + "\'lastname\': " + "\'" + last + "\'"
		else:
			payload = payload + ", \'lastname\': " + "\'" + last + "\'"
	
		if totalprice != "" or depositpaid != "" or additionalneeds != "":		
			payload = payload + ", "

	if totalprice:
		payload = payload + "\'totalprice\': " + "\'" + totalprice + "\', "

	if depositpaid:
		payload = payload + "\'depositpaid\': " + "\'" + depositpaid + "\', "

	if checkin:
		if comma_flag == False or depositpaid != "":
			payload = payload + "\'bookingdates\': {\'checkin\': " + "\'" + checkin + "\'"
		else:
			payload = payload + ", \'bookingdates\': {\'checkin\': " + "\'" + checkin + "\'"
		if checkout == "":
			payload = payload + "}"
		comma_flag = True

	if checkout:
		if comma_flag == False:
			if checkin == "":
				payload = payload + "\'bookingdates\': {\'checkout\': " + "\'" + checkout + "\'}"
			else:
				payload = payload + "\'checkout\': " + "\'" + checkout + "\'}"
		else:
			if checkin == "":
				payload = payload + ", \'bookingdates\': {\'checkout\': " + "\'" + checkout + "\'}"
			else:
				payload = payload + ", \'checkout\': " + "\'" + checkout + "\'}"

	if additionalneeds:
		payload = payload + ", \'additionalneeds\': " + "\'" + additionalneeds + "\'"
	
	payload = payload + "}"
	return payload


def find_params(the_payload, the_response):
	global bookings
	#bookings = []
	torf = True
	for param in the_payload:		
		match = re.search(the_payload[param], the_response)
		if match:
			pass
		else:
			torf = False
			return torf
	return torf


def convert_payload_string_to_dict(the_payload):
	the_payload = re.sub('\'', '"', str(the_payload))
	the_payload = re.sub(' ', '', str(the_payload))
	the_payload = re.sub('"bookingdates":{', '', str(the_payload))
	the_payload = re.sub('}}', '}', str(the_payload))
	the_payload = re.sub('},"', ',"', str(the_payload))
	the_payload = json.loads(the_payload)
	return the_payload							# this converts payload from a string to a dictionary


def get_booking(first = "", last = "", totalprice = "", depositpaid = "", checkin = "", checkout = "", additionalneeds = ""):
	# according to restful-booker site, first and last names and checkin and checkout are all optional
	passed = True
	bookings = []
	# this first section builds up the payload based on the parameters passed to the function
	payload = build_payload(first, last, "", "", checkin, checkout, "")
	print("Here are the derived values of payload: ", payload)

	# create the list of GetBookingIds:
	response = requests.get('https://restful-booker.herokuapp.com/booking/', params=payload)
	response_code(response.status_code)					# was an error returned?
	ids = re.findall('\d+', response.text)				#ids is a list of integers corresponding to the booking IDs
	bookings = []
	bookings.append("There are " + str(len(ids)) + " bookings found in the system:")
	payload = convert_payload_string_to_dict(payload)
	# this loop gets the actual bookings (based on the IDs), and then uses the params passed to the function 
	# to see if they are correct
	for i in ids:
		s = 'https://restful-booker.herokuapp.com/booking/' + str(i)
		response = requests.get(s)
		response_code(response.status_code)				 # was an error returned?
		bookings.append("\t" + str(response.text))
		torf = find_params(payload, str(response.text))
		if torf == True:
			bookings.append("Found all get_booking parameters in one of the bookings.  Test passed.")
			return bookings
	bookings.append("Could not find all the get_booking parameters in any of the bookings.  Test FAILED!")
	return bookings


def confirm_booking(the_payload, response):
	global passed
	the_payload = convert_payload_string_to_dict(the_payload)
	bookings = []  					
	for param in the_payload:	
		match = re.search(the_payload[param], response)
		if match:
			pass
		else:
			passed = False
			print(the_payload[param], response)
			bookings.append("Couldn't find " + str(param) + " in the booking.  Test FAILED!")
			return bookings
	bookings.append("All checks passed.")
	return bookings


def create_booking(firstname, lastname, totalprice, depositpaid, checkin, checkout, additionalneeds, confirm, id, return_data):
	passed = True 
	i = ""                                                         
	payload = {"firstname" : firstname, "lastname" : lastname, "totalprice" : totalprice, "depositpaid" : depositpaid, "bookingdates" : {"checkin" : checkin, "checkout" : checkout}, "additionalneeds" : additionalneeds}

	response = requests.post('https://restful-booker.herokuapp.com/booking/', json=payload)				# make the booking
	response_code(response.status_code)																	# was an error returned?
	match = re.search('\"bookingid\":(\d+)', response.text)												# this is for getting the ID number to use for getting the booking
	if match:
		i = match.group(1)
	s = 'https://restful-booker.herokuapp.com/booking/' + str(i)
	response = requests.get(s)
	response_code(response.status_code)		
	if confirm == True:
		results = confirm_booking(payload, response.text)
	if return_data == "bookings" and confirm == True:
		return results
	else:
		return i


def update_booking(firstname, lastname, totalprice, depositpaid, checkin, checkout, additionalneeds, confirm, id, return_data):
	passed = True
	i = ""                                                         
	payload = {"firstname" : firstname, "lastname" : lastname, "totalprice" : totalprice, "depositpaid" : depositpaid, "bookingdates" : {"checkin" : checkin, "checkout" : checkout}, "additionalneeds" : additionalneeds}
	auth_token = get_authtoken()																	
	s = 'https://restful-booker.herokuapp.com/booking/' + str(id) + ', json=payload, cookies={"token" : auth_token}'
	response = requests.put(s)
	if confirm == True:
		results = confirm_booking(payload, response.text)
	if return_data == "bookings" and confirm == True:
		return results
	else:
		return i


def delete_booking(id):
	auth_token = get_authtoken()																	
	s = 'https://restful-booker.herokuapp.com/booking/' + str(id) + ', json=payload, cookies={"token" : auth_token}'
	response = requests.delete(s)
