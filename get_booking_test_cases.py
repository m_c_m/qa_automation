# These tests all first create a booking, to give a fixed set of data, and then gets the booking and confirms it's correct

import restful_booker_api
import sys
import time

def writeit(*params):						# this function just prints *params to both the console and the log file
	for i in params:
		print(i)
		f.write(str(i))
		#f.write(" \n")
	#print("\n")
	f.write("\n")


try:				# Can the file be opened?
	f = open("REST_API_Test_Log.txt", "a+")					# open the test log in append mode with handle 'f'
except IOError:			# No!  The file could not be opened.
	writeit("Couldn't open REST_API_Test_Log.txt", f)
	sys.exit()			# Die.

writeit("\n\n\n############################################################################")
writeit("This set of tests for the GetBooking endpoint was executed at {}.\n".format(time.ctime(time.time())))		# log the time

################### test case 1 for GetBooking endpoint ###################
writeit("*** Test for first name, last name, checkin date, checkout date:")
restful_booker_api.create_booking("Aria", "Shen", 250, "true", "2020-09-02", "2020-09-03", "view of the lake", False, "0", "bookings")
writeit(*(restful_booker_api.get_booking("Aria", "Shen", 250, "true", "2020-09-02", "2020-09-03", "view of the lake")))

################### test case 2 for GetBooking endpoint ###################
writeit("\n*** Test for first name, last name:")
writeit(*(restful_booker_api.get_booking("Aria", "Shen")))

################### test case 3 for GetBooking endpoint ###################
writeit("\n*** Test only for last name:")
writeit(*(restful_booker_api.get_booking("", "Shen")))

################### test case 4 for GetBooking endpoint ###################
writeit("\n*** Test for checkin date, checkout date:")
writeit(*(restful_booker_api.get_booking("", "", "", "", "2020-09-02", "2020-09-03")))

################### test case 5 for GetBooking endpoint ###################
writeit("\n*** Negative test with known bad data.  This should result in a fail:")
writeit(*(restful_booker_api.get_booking("Negative", "Test", "1000-04-05", "3000-07-12")))


f.close()