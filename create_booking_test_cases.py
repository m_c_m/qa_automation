import restful_booker_api
import sys
import time

def writeit(*params):						# this function just prints *params to both the console and the log file
	for i in params:
		print(i)
		f.write(str(i))
		#f.write(" \n")
	#print("\n")
	f.write("\n")


try:				# Can the file be opened?
	f = open("REST_API_Test_Log.txt", "a+")					# open the test log in append mode with handle 'f'
except IOError:			# No!  The file could not be opened.
	writeit("Couldn't open REST_API_Test_Log.txt", f)
	sys.exit()			# Die.

writeit("\n\n\n############################################################################")
writeit("This set of tests for the CreateBooking endpoint was executed at {}.\n".format(time.ctime(time.time())))		# log the time

################### test case 1 for CreateBooking endpoint ###################
writeit("*** Standard, well-behaved case:")
writeit(*(restful_booker_api.create_booking("Aria", "Shen", "250", "true", "2020-09-02", "2020-09-03", "view", True, "0", "bookings")))

################### test case 2 for CreateBooking endpoint ###################
writeit("\n*** Error in writing out the cost (should fail):")
writeit(*(restful_booker_api.create_booking("John", "Lennon", "abc", "true", "1980-09-02", "2020-09-03", "penthouse", True, "0", "bookings")))

################### test case 3 for CreateBooking endpoint ###################
writeit("\n*** Not prepaid, no special requirements:")
writeit(*(restful_booker_api.create_booking("William", "Shakespeare", "1", "true", "1598-09-02", "2020-09-03", "", True, "0", "bookings")))

