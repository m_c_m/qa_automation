# I have not yet successfully been able to use an authentication token to allow me to delete a booking.  
# Therefore, this test is not yet working.  

import restful_booker_api
import sys
import time

def writeit(*params):						# this function just prints *params to both the console and the log file
	for i in params:
		print(i)
		f.write(str(i))
		#f.write(" \n")
	#print("\n")
	f.write("\n")


try:				# Can the file be opened?
	f = open("REST_API_Test_Log.txt", "a+")					# open the test log in append mode with handle 'f'
except IOError:			# No!  The file could not be opened.
	writeit("Couldn't open REST_API_Test_Log.txt", f)
	sys.exit()			# Die.

writeit("\n\n\n############################################################################")
writeit("*** This set of tests for the DeleteBooking endpoint was executed at {}.\n".format(time.ctime(time.time())))		# log the time

################### test case 1 for DeleteBooking endpoint ###################
writeit("Create the booking, delete it and verify it's gone:")
id = restful_booker_api.create_booking("John", "Lennon", 1000, True, "1980-09-02", "2020-09-03", "penthouse", False, "0", "id")  # create a booking and return its ID
restful_booker_api.delete_booking(id)					# now delete the booking
booking = restful_booker_api.get_single_booking(id)	
if booking == "Not Found":
	writeit("The booking no longer exists.  Test passed.")
else:
	writeit("The booking was not successfully deleted.  Test FAILED!")

################### test case 2 for DeleteBooking endpoint ###################
writeit("\n*** Create the booking, delete a different booking and verify the original is still present:")
id = restful_booker_api.create_booking("Paul", "McCartney", 1500, True, "2020-09-02", "2030-09-03", " top penthouse", False, "0", "id")  # create a booking and return its ID
restful_booker_api.delete_booking(1)					# now delete the booking?
booking = restful_booker_api.get_single_booking(id)		# can we get it?  we should
if booking != "Not Found":
	writeit("The booking still exists.  Test passed.")
else:
	writeit("The booking no longer exists.  Test FAILED!")
